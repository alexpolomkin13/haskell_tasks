module Division where 

sumInteger :: (Integer, Integer) -> (Integer, Integer) -> (Integer, Integer) 

sumInteger x y = (fst x + fst y, snd x + snd y) 
 
multiplicationInteger :: (Integer, Integer) -> (Integer, Integer) -> (Integer, Integer) 
multiplicationInteger x y = (fst x * fst y, snd x * snd y) 

makeDiv :: Integer -> Integer -> (Integer, Integer) 

makeDiv a b | (b == 0) = error "cannot be divided by zero" 
makeDiv a b | (a > 0 && b < 0) = multiplicationInteger (-1, 1) (makeDiv a (-1 * b)) 
makeDiv a b | (a < 0 && b > 0) = multiplicationInteger (-1, -1) (makeDiv (-1 * a) b) 
makeDiv a b | (a < 0 && b < 0) = multiplicationInteger (1, -1) (makeDiv (-1 * a) (-1 * b))
makeDiv a b | (a < b) = (0,a) 
makeDiv a b | (a >= b) = sumInteger (1, 0) (makeDiv (a - b) b)
